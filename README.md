# bronze-mamba

## 1. What is the difference between a pod and deployment in Kubernetes?

Pods and Deployments are closely related Kubernetes API objects.
A Pod is a co-located, logical grouping of one or more containers
that run in a shared context, with shared network and storage resources.
Pods allow multiple containers to be defined as a single unit, making it
possible to horizontally scale complex groupings of containerized software
components.

Pods can be created directly by Kubernetes administrators, but they are
typically created by a workload resource, such as a Deployment.
Deployments define the behavior, quantity, and attributes
of resources used by Pods. A Deployment spec describes a desired
state of resources, and the Deployment controller enforces
the actual state of the cluster.

## 2. A user on a RHEL based machine runs "rpm -i packagename.rpm". Describe what happens during the lifecycle of this command.

RPM performs the following steps when running `rpm -i packagename.rpm`:

1. Checks the package SPEC file.
1. Checks the system for dependencies defined in the `Requires` directive.
1. Checks for conflicts, for example attempting to install a package that already exists on the system.
1. Performs any preinstall tasks for the package, as defined in the `%pre` directive.
1. Checks for config files defined in the `%config` of `%config(noreplace)` directives and provides special handling.
1. Unpacks files and copies them to their target locations on disk, as defined in the `%install` directive.
1. Performs any postinstall actions defined in the `%post` directive.
1. Writes information to the RPM database about the installation, including locations of copied files.

## 3. Describe a challenge and advantage of both monolithic and microservice architectures.

One challenge of monolithic architectures is that services are tightly-coupled;
they run on the same server. To scale monolithic applications, additional servers
are added to provide increased capacity. Tight coupling of services makes
monolithic architectures less fault-tolerant, and makes precise scaling more
challenging because all services must be scaled together as a unit. On the other hand,
monolithic architectures can be simpler to configure and easier to reason about,
as services share a common context.

A challenge of microservice architectures is the increased complexity
involved in independently managing loosely-coupled services. For example,
in a monolithic architecture, services typically operate within the same
trust boundary, i.e. services can inherently communicate with one another
securely without needing additional authentication (authn) or authorization
(authz). On the other hand, in a microservices architecture, each service
may need to perform authz/authn to any other service, as they are not necessarily
operating within a shared trust boundary; they may be running on different servers
or in different data centers. This difference may necessitate an additional system
for authz/authn in microservice architectures vs monolithic architectures.

Despite increased complexity, microservice architectures have their advantages.
For example, because application components are loosely coupled, capacity of
individual services can be independently scaled up or down in order to efficiently
utilize resources. Microservice architectures also provide technological flexibility;
teams are not limited to using the same framework on language for the entire application.

## 4. Can you write a small Ruby based script that will get HTTP response times over 5 minutes from your location to https://gitlab.com?

I wrote two scripts.

### `main.rb`

I wanted to provide a high-quality solution to this problem; this is part of an interview. This script is far from perfect, but it has many of the pieces I'd want to see if I were going to operationalize a script like this.

To run this script locally:

`docker run -it registry.gitlab.com/call/bronze-mamba`

To see the output of scheduled runs in GitLab CI, see the [pipeline schedules](https://gitlab.com/call/bronze-mamba/-/pipeline_schedules)
for this repository. Of course, when running in GitLab CI, load times are from GitLab's infrastructure, and are about twice as fast as from my location!

### `small.rb`

While I was finishing this questionnaire, I felt I should also satisfy the literal requirements of this question. This script is as small as I could get.

To run this script locally, run `ruby small.rb`.

## 5. Describe some advantages and challenges to working remotely.

### Advantages

- Ability to have a globally diverse workforce, which can provide invaluable
  breadth of perspective that helps create the most inclusive product possible.

- Egalitarian. Especially with a fully remote company, there is no
  concentration of power or focus around headquarters, a dynamic that can
  create a sense of "us and them" between HQ and non-HQ employees. For both
  better and worse, there are no "watercooler/hallway conversations."

- More efficient use of time. No commute, better time flexibility in general
  (may depend on the role).

- An environment that suits the employee. At both a micro (workspace) and
  macro (geography) level, the ability for employees to control their
  environment can lead to employee happiness and help employees work their best.

### Challenges

- Digital interaction is less natural than physical, in-person interaction.
  You must put forth extra effort and be creative in finding ways
  to stay engaged and connected with colleagues.

- Work/life balance can suffer. Because work is not time-boxed by a commute,
  or forced to happen at a central location, it can be easy to habitually let
  work bleed into time that should be spent on other things.

- Doesn't work for everyone. Some personalities or work styles are not well-suited
  to working remotely. This goes both ways though; I vastly prefer remote
  work and find my work style is better-suited to 100% remote work than 100% in-person.
  There is no one-size-fits-all approach and this goes for remote work as well.

## 6. Aside from Ruby, are there any other programming languages you have significant experience with?

| Context             | Languages         |
|---------------------|-------------------|
| Professional        | Python, Ruby      |
| Academic, Graduate  | Swift, JavaScript |
| Academic, Undergrad | C++               |

## 7. Describe your deployment automation and packaging experience.

### Deployment automation experience

#### Puppet

We used [`r10k`](https://github.com/voxpupuli/puppet-r10k) and Jenkins to automatically
deploy branches as Puppet environments whenever a push occurred in GitHub.

#### Kubernetes

At Puppet, I ran Kubernetes clusters dedicated to
[Apache Airflow](https://airflow.apache.org/), and used
Jenkins to perform Helm deploys to development, staging, and production clusters.
I used [Helmfile](https://github.com/roboll/helmfile) extensively to codify
and automate secrets deployment, per-environment configuration,
namespace and context selection, and other variable aspects of deployments.

#### Static websites

I use GitHub Actions to build and deploy Hugo and Jekyll sites to GitHub Pages,
both personally and for internal documentation.

#### Client CI/CD at Salesforce

At Salesforce, I've build deployment pipelines that support over 70,000 nodes.

##### Package deployment

For Linux, we use AWS Lambda, triggered via GitHub webhook,
to build Debian packages from source and serve them up via S3.

For macOS, we've leveraged [autopkg](https://github.com/autopkg/autopkg)
and [Jssimporter](https://github.com/jssimporter/JSSImporter),
paired with some custom automation,
to fetch and repackage vendor software and upload it to our MDM server daily.
Automated package promotion through several stages, from canary
deployments to production, is performed via a scheduled pipeline.

##### Testing deployments

The process of deploying and testing new configuration on physical workstations
has historically been a huge burden for my team. This year we've fully
virtualized this process using VirtualBox, Veertu Anka, Hashicorp Packer,
and Buildkite.

Engineeers write Serverspec tests for configuration changes before making the
changes in Puppet or our MDM server. On each Git push, a virtual machine is
created by Buildkite, then provisioned and tested using the
code in the branch. Topic branch VM deployments with Serverspec tests
provide smoke tests for configuration changes. We also deploy production
config and tests to VMs hourly to ensure our deployment process stays healthy.

### Packaging Experience

My packaging experience is macOS and Docker-heavy. For the better part of a
decade, I've been involved with every aspect of macOS packaging,
from building custom packages to deciphering unidiomatic vendor packages, to
building best-practice packaging automation for a Fortune 500 company.

My roughly five years of Docker experience has centered around
building developer tooling images for my teams,
building [ETL](https://en.wikipedia.org/wiki/Extract,_transform,_load) images
for Apache Airflow, and managing a custom Apache Airflow image based on
[puckel/docker-airflow](https://github.com/puckel/docker-airflow) before
there was a community standard.

I also created the Apache Airflow Helmfile project and Helm chart that is
still in use internally at Puppet.

## 8. Do you have an open source project that you own or contributed to that you feel particularly proud about

### Python library: ultipro-soap-python

An old example, but one I'm proud of, is the first project
I created as a developer at Puppet—a small Python library named [`ultipro-soap-python`](https://github.com/puppetlabs/ultipro-soap-python).

UltiPro is the [HRIS](https://en.wikipedia.org/wiki/Human_resource_management_system)
that Puppet used. UltiPro had an archaic and arcane SOAP API, with poor
documentation (in Word docs), and no sandbox environment.
I was able to decipher this API and deliver a sustainable integration using modern tooling, where senior and principal engineers had failed before me. Needless to say, I was chuffed.
The library was open sourced shortly before I left the team, and I've been
pleased to see it's been used by a handful of developers around the world.

### Puppet module: puppet-buildkite_agent

More recently, I shipped a feature-complete Puppet module for managing
[Buildkite Agent](https://buildkite.com/docs/agent/v3) on macOS.
The project can be found [on GitHub](https://github.com/call/puppet-buildkite_agent)
and [on the Puppet Forge](https://forge.puppet.com/modules/call/buildkite_agent).

### Giving back

In general, I try to give back to open source projects I use.
My contributions are most often documentation, but in 2020 I contributed to
Veertu, Buildkite, Specinfra, and Munki projects.
See [my 2020 contributions on GitHub](https://github.com/call?tab=overview&from=2020-12-01&to=2020-12-31) for details.
