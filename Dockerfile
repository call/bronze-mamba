FROM ruby:2.5-alpine
WORKDIR /usr/src/app
COPY main.rb .
CMD ["ruby", "main.rb"]
