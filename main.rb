# frozen_string_literal: true

require "net/http"
require "uri"
require "logger"

LOGLEVELS = %w[DEBUG INFO WARN ERROR FATAL UNKNOWN].freeze
LOGGER = Logger.new($stdout)
level ||= LOGLEVELS.index ENV.fetch("LOG_LEVEL", "INFO")
level ||= Logger::INFO
LOGGER.level = level

RUN_SECONDS = ENV.fetch("RUN_SECONDS", 300).to_i
TIMEOUT_SECONDS = ENV.fetch("TIMEOUT_SECONDS", 10).to_i
INTERVAL_SECONDS = ENV.fetch("INTERVAL_SECONDS", 10).to_i
SPLAY = ENV.fetch("SPLAY", "true")

LOGGER.debug("RUN_SECONDS=#{RUN_SECONDS}")
LOGGER.debug("TIMEOUT_SECONDS=#{TIMEOUT_SECONDS}")
LOGGER.debug("INTERVAL_SECONDS=#{INTERVAL_SECONDS}")
LOGGER.debug("SPLAY=#{SPLAY}")

# This class profiles GET requests to GitLab.com
class GitlabGetProfiler
  include Process

  def fetch(uri_str)
    url = URI.parse(uri_str)
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.max_retries = 0 # Avoid default Net:HTTP retry for idempotent HTTP methods
    http.open_timeout = TIMEOUT_SECONDS
    http.read_timeout = TIMEOUT_SECONDS
    http.start

    request = Net::HTTP::Get.new(url.path)
    response = http.request(request)

    case response
    when Net::HTTPSuccess then response
    when Net::HTTPRedirection then fetch(response["location"])
    else
      response.error!
    end
  end

  def log_run_summary(rtts, count, run_start)
    raise "All tests errored!" if rtts.empty?

    duration = clock_gettime(CLOCK_MONOTONIC) - run_start
    test_success_count = rtts.size
    test_error_count = count - test_success_count
    run_max = rtts.max
    run_min = rtts.min
    run_avg = rtts.reduce(:+) / test_success_count.to_f

    count_msg = "%-20<label>s%<count>s"
    seconds_msg = "%-20<label>s%.2<seconds>fs"

    LOGGER.info("--- Run Summary ---")
    LOGGER.info(seconds_msg % [label: "Duration", seconds: duration])
    LOGGER.info(count_msg % [label: "Try count", count: count])
    LOGGER.info(count_msg % [label: "Success count", count: test_success_count])
    LOGGER.info(count_msg % [label: "Error count", count: test_error_count])
    LOGGER.info(seconds_msg % [label: "Max RTT", seconds: run_max])
    LOGGER.info(seconds_msg % [label: "Min RTT", seconds: run_min])
    LOGGER.info(seconds_msg % [label: "Avg RTT", seconds: run_avg])
  end

  def custom_sleep
    seconds = if SPLAY == "true"
                rand(0..(2 * INTERVAL_SECONDS))
              else
                INTERVAL_SECONDS
              end

    return if seconds.zero?

    LOGGER.info("[sleep] %.2<seconds>fs" % [seconds: seconds])
    sleep(seconds)
  end

  def run
    LOGGER.debug("Run start.")
    run_start = clock_gettime(CLOCK_MONOTONIC)
    rtts = []
    i = 1

    loop do
      begin
        fetch_start = clock_gettime(CLOCK_MONOTONIC)
        fetch("https://gitlab.com/")
        rtt = (clock_gettime(CLOCK_MONOTONIC) - fetch_start)
        rtts.append(rtt)
        LOGGER.info("[%05<count>d] %.2<time>fs" % [count: i, time: rtt])
      rescue StandardError => e
        LOGGER.error("[%05<count>d] %<error>s" % [count: i, error: e])
      end

      break if (clock_gettime(CLOCK_MONOTONIC) - run_start) >= RUN_SECONDS

      custom_sleep
      i += 1
    end

    log_run_summary(rtts, i, run_start)
  end
end

job = GitlabGetProfiler.new
job.run
